
## Flutter 兴趣小组

Flutter SIG 致力于为推进 Flutter 应用开发框架在 openKylin 社区的支持，包括对 Flutter 开发环境支持和 Flutter 应用运行支持。


## 工作目标

- 提供开箱即用的 Flutter 应用开发环境，支持Android、Linux 等应用开发环境
- 完善 openkylin 对 Flutter应用支持，包括对多媒体/窗口/通知等兼容性
- 完善对 openKylin 上的相关运行的相关文档、示例、 codelabs 等


## SIG成员

### Owner

李何佳[@lihejia](https://gitee.com/lihejia) ([lihejia@syberos.com](mailto:lihejia@syberos.com))

### Maintainer列表

| Maintainer                                              | 邮箱                                                        |
| ------------------------------------------------------- | ----------------------------------------------------------- |
| 王亮[@abai-b](https://gitee.com/abai-b)     | [wangliang@syberos.com](mailto:wangliang@syberos.com)     |
| 马万郡[@xiaolandan](https://gitee.com/xiaolandan)           | [mawanjun@syberos.com](mailto:mawanjun@syberos.com)                       |
| 唐俊崇[@tang-junchong](https://gitee.com/tang-junchong)         | [tangjunchong@syberos.com](mailto:tangjunchong@syberos.com) |
| 王大为[@davidwongiiss](https://gitee.com/davidwongiiss)         | [wangdawei@syberos.com](mailto:wangdawei@syberos.com)               |

## SIG维护包列表

- 
...

## SIG邮件列表
> flutter@lists.openkylin.top
