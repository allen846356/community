
## OpenSDK SIG

本sig组负责开发者套件（base、system、applications）规划、开发、维护等工作，致力于解决应用在多操作系统中的兼容性问题。

## 工作目标

- kysdk-base层开发
- kysdk-system层开发
- kysdk-applications层开发

## SIG成员
### Owner
- 刘云鹤 liuyunhe@kylinos.cn
- 陈志开 chenzhikai@kylinos.cn
- 孙振 sunzhen@kylinos.cn
### Maintainers
- 刘云鹤 liuyunhe@kylinos.cn
- 陈志开 chenzhikai@kylinos.cn
- 孙振 sunzhen@kylinos.cn
- 邵智敏 shaozhimin@kylinos.cn
- 王巍然 wangweiran@kylinos.cn
- 刘相伟 liuxiangwei@kylinos.cn
- 田绍帅 tianshaoshuai@kylinos.cn

## 邮件列表
opensdk@lists.openkylin.top
