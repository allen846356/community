## 音频SIG组

### 工作目标

本SIG组致力于系统声音,推进系统声音在社区维护,应用社区相关新技术

### SIG成员
- fengzhaoxiang@kylinos.cn
- zuozhiwei@kylinos.cn
- pengziping@kylinos.cn

### SIG维护包列表
- alsa-ucm-conf
- alsa-lib
- pulseaudio
- ukui-media
