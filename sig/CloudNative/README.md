## CloudNative SIG

CloudNative SIG 组致力于将云原生技术开放和适配到openKylin系统上，方便社区用户快速上手云原生技术栈


## 工作目标

- 负责云原生技术栈相关项目的规划、开发、升级与维护
- 及时响应社区用户对项目使用过程中碰到的问题


## SIG成员

Owner
- [钟生平](zhong_shengping)


Maintainer
- [薛晋泽](xuejinze1)
- [曹远志](dancewhale)

## SIG维护包列表

- cilium
- kse
- kse-rescheduler

## 路标
23年底
- 完成SIG建立
- 完成首批项目建立


## SIG邮件列表
> 格式：SIG名小写@lists.openkylin.top（SIG申请审核通过后，管理员将创建该邮件列表）

## SIG组例会
> 明确SIG组例会召开时间和周期，如周例会、双周例会或月会等